<?php 
session_start();
include("fonctions.php");
$concerts = getTourById($_GET["id"]);
$daty = NULL;
$pays = NULL;
$lieu = NULL;
foreach($concerts as $concert){
	$daty = $concert['daty'];
	$pays = $concert['pays'];
	$lieu = $concert['lieu'];
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
          <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
          <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
          <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
          <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
          <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

        <link rel="stylesheet" type="text/css" href="sweetalert2/dist/sweetalert2.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition login-page">
		<div class="login-box">
           <div class="login-box-body">
             <p class="login-box-msg">MODIFIER CONCERT</p>
             <form action="page_concert_mtraitement.php" method="post">
				<input type="hidden" name="idChanson" value="<?php echo $_GET['id'];?>" required>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">DATY:</span>
						<input type="date" class="form-control" placeholder="Date" name="daty" value="<?php echo $daty;?>">
					</div>
                 </div>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">PAYS:</span>
						<input type="text" class="form-control" placeholder="Pays" name="pays" value="<?php echo $pays;?>">
					</div>
                 </div>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">LIEU:</span>
						<input type="text" class="form-control" placeholder="Lieu" name="lieu" value="<?php echo $lieu;?>">
					</div>
                 </div>
                 <div class="row">
                   <div class="col-xs-8">
                   </div>
                   <!-- /.col -->
                   <div class="col-xs-4">
                      <button type="submit" class="btn btn-primary btn-block btn-flat" name="Login">MODIFIER</button>
                   </div>
                   <!-- /.col -->
                 </div>
             </form>
           </div>
        </div>
    </body>
</html>
