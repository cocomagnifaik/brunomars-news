<?php 
session_start();
include("fonctions.php");
$tours = getTourPage();
$image = NULL;
$alt = NULL;
$h1 = NULL;
$h2 = NULL;
$h22 = NULL;
$image1 = NULL;
$alt1 = NULL;
$image2 = NULL;
$alt2 = NULL;
$url = NULL;
foreach($tours as $tour){
	$image = $tour['image'];
	$alt = $tour['alt'];
	$h1 = $tour['h1'];
	$h2 = $tour['h2'];
	$h22 = $tour['h22'];
	$image1 = $tour['image1'];
	$alt1 = $tour['alt1'];
	$image2 = $tour['image2'];
	$alt2 = $tour['alt2'];
	$url = $tour['url'];
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
          <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
          <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
          <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
          <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
          <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

        <link rel="stylesheet" type="text/css" href="sweetalert2/dist/sweetalert2.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition login-page">
	<section class="content">
		<section class="col-md-12">
		  <div class="box box-info">
			<div class="small-box bg-red col-md-6">
				<h4><strong>Inserer d'abord image dans les champs suivants:</strong></h4><br>
			  <form name="validation">
				  <progress value="0" max="100" id="uploadProgress">0</progress> Veuillez attendre la fin du telechargement de l' <strong>image</strong> avant de "Valider votre image"!
				<input type="file" class="form-control" name="fichier" id="fichier" placeholder="Image" required>
				<br>
				<button type="button" id="valid" onclick="go()" style="background: #005983" data-widget="remove">Valider votre image</button>
			  </form><br>
			</div>
		  </div>
		</section>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyDZWbV5WwKOIEwRZ0Nq9POO2YKxrLxaw3M",
    authDomain: "brunomarsnews-512fe.firebaseapp.com",
    databaseURL: "https://brunomarsnews-512fe.firebaseio.com",
    projectId: "brunomarsnews-512fe",
    storageBucket: "brunomarsnews-512fe.appspot.com",
    messagingSenderId: "1002209203404"
  };
  firebase.initializeApp(config);
  var storage = firebase.storage();  
  var progress = document.getElementById('uploadProgress') 
  var button = document.getElementById('fichier')
  var valid = document.getElementById('valid')
    button.addEventListener('change',function(e){
	var file = e.target.files[0];
	var storageRef = storage.ref(file.name)
	var uploadTask = storageRef.put(file)	
	uploadTask.on('state_changed',loadUpload,errUpload,completeUpload)	
	function loadUpload(data){
		var percent = (data.bytesTransferred/data.totalBytes) * 100
		progress.value = percent
	}	
	function errUpload(err){
		console.log('error')
		console.log(err)
	}
	function completeUpload(data){
		alert('Succes')
		console.log('success')
		console.log(data)
	}
  })
  var refDown = null
    function go(){
        var ato = document.getElementById('fichier').value
        var tableau = ato.split("\\")
        refDown = storage.ref(tableau[2])
        alert('Fichier mp3 valide!')
    }
        valid.addEventListener('click',function(){
            refDown.getDownloadURL().then(function(url){
                console.log("MP3 download")
                console.log(url)
                document.getElementById("image").value=url
            })
        })
</script>
		<div class="login-box">
           <div class="login-box-body">
             <p class="login-box-msg">MODIFIER TOUR</p>
             <form action="page_tour_mtraitement.php" method="post">
				<input type="hidden" id="image" name="image" value="<?php echo $image;?>" required>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">ALT:</span>
						<textarea rows="2" class="form-control" placeholder="Alt" name="alt"><?php echo $alt;?></textarea>
					</div>
                 </div>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">H1:</span>
						<textarea rows="2" class="form-control" placeholder="H1" name="h1"><?php echo $h1;?></textarea>
					</div>
                 </div>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">H2:</span>
						<textarea rows="2" class="form-control" placeholder="H2" name="h2"><?php echo $h2;?></textarea>
					</div>
                 </div>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">H2`:</span>
						<textarea rows="2" class="form-control" placeholder="H2`" name="h22"><?php echo $h22;?></textarea>
					</div>
                 </div>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">IMAGE1:</span>
						<textarea rows="5" class="form-control" placeholder="image1" name="image1"><?php echo $image1;?></textarea>
					</div>
                 </div>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">ALT1:</span>
						<textarea rows="2" class="form-control" placeholder="Alt1" name="alt1"><?php echo $alt1;?></textarea>
					</div>
                 </div>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">IMAGE2:</span>
						<textarea rows="5" class="form-control" placeholder="image2" name="image2"><?php echo $image2;?></textarea>
					</div>
                 </div>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">ALT2:</span>
						<textarea rows="2" class="form-control" placeholder="Alt2" name="alt2"><?php echo $alt2;?></textarea>
					</div>
                 </div>
                 <div class="form-group has-feedback">
                    <div class="input-group">
						<span class="input-group-addon" id="sizing-addon-2">URL:</span>
						<textarea rows="2" class="form-control" placeholder="URL" name="url"><?php echo $url;?></textarea>
					</div>
                 </div>
                 <div class="row">
                   <div class="col-xs-8">
                   </div>
                   <!-- /.col -->
                   <div class="col-xs-4">
                      <button type="submit" class="btn btn-primary btn-block btn-flat" name="Login">MODIFIER</button>
                   </div>
                   <!-- /.col -->
                 </div>
             </form>
           </div>
        </div>
    </body>
</html>
