<?php 
session_start();
include("fonctions.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
          <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
          <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
          <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
          <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
          <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

        <link rel="stylesheet" type="text/css" href="sweetalert2/dist/sweetalert2.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    
    <body class="hold-transition login-page">
<?php
		$users = getUsers();
		if(isset($_POST["email"]) && isset($_POST["password"])){
			foreach($users as $user){
				if($_POST["email"] == $user["email"] && sha1($_POST["password"]) == $user["password"]){
				$_SESSION["idUser"] = $user["id"];					
?>
			<script src="sweetalert2/dist/sweetalert2.min.js"></script>
			<script type="text/javascript">
				swal("Bienvenue", "Votre etes un admin!", "success");
				document.location.href='page_musique.php'
			</script>
		<?php   }
				else { ?>
			<script src="sweetalert2/dist/sweetalert2.min.js"></script>
			<script type="text/javascript">
				sweetAlert("Erreur", "Vous n'etes pas admin ou votre mot de passe est incorrect!", "error");
			</script>
		<?php	}
			}
		}
?>
		<div class="login-box">
         <div class="login-logo">
           <a href="index.php"><b>B.M</b> News</a>
         </div>
           <div class="login-box-body">
             <p class="login-box-msg">Connectez-vous pour ouvrir une session</p>
             <form action="index.php" method="post">
                 <div class="form-group has-feedback">
                   <input type="email" class="form-control" placeholder="Email" name="email" required>
                   <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                 </div>
                 <div class="form-group has-feedback">
                   <input type="password" class="form-control" placeholder="Mot de passe" name="password" required>
                   <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                 </div>
                 <div class="row">
                   <div class="col-xs-8">
                   </div>
                   <!-- /.col -->
                   <div class="col-xs-4">
                      <button type="submit" class="btn btn-primary btn-block btn-flat" name="Login">Connection</button>
                   </div>
                   <!-- /.col -->
                 </div>
             </form>
            <script src="sweetalert2/dist/sweetalert2.min.js"></script>
               <a href="register.php" class="text-center">S'inscrire pour etre admin</a>
           </div>
        </div>
    </body>
</html>
