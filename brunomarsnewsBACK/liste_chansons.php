<!DOCTYPE html>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TiaCaraks | Chansons</title>
          <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
          <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
          <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
          <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
          <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include file="header.php"; ?>

  <!-- Left side column. contains the logo and sidebar -->
    <?php include file="aside.php" ?>  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Chanson
        <small>Insertion</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="page_musique.jsp"><i class="fa fa-dashboard"></i> Accueil</a></li>
        <li class="active">Chanson</li>
      </ol>
    </section>
        <%
            MusiqueDAO musique = new MusiqueDAO();
            Musique[] mus = musique.listMusique();
        %>
    
    <section class="content">
        <section class="col-md-12" style="margin-bottom: 50px">
            <div class="box-body col-md-12" style="background: #003147; color: #FFF">
                <h4><strong>Liste des chansons</strong></h4>
                <table class="table table-sm table-dark">
                <% for(int i = 0; i < mus.length; i++) { %>                        
                <tbody>
                    <tr>
                        <td>Artiste</td>
                        <td>Chanson</td>
                        <td>Paroles</td>
                        <td>Minutages et Reponses</td>
                    </tr>
                    <tr class="table-primary">
                        <td class="table-info"><img src='<% out.print(mus[i].getImage()); %>' width="80px"></br>
                        </td>
                        <td><% out.print(mus[i].getArtiste()); %> - 
                        <% out.print(mus[i].getTitre()); %></br><audio src="<% out.print(mus[i].getFichier()); %>" controls></audio></td>
                        <td><% out.print(mus[i].getParoles()); %></td>
                        <td><strong>Minutages:</strong><% out.print(mus[i].getMinutage()); %><br><strong>Valiny:</strong><% out.print(mus[i].getValiny()); %></td>
                    </tr>
                </tbody>
                <% } %>
                </table>
            </div>
        </section>
        
    </section>
    </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Project by</b> Tsiory & Cedrick
    </div>
    <strong>Copyright &copy; 2018.</strong> All rights
    reserved.
  </footer>
</div>

<!-- jQuery 3 -->
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>