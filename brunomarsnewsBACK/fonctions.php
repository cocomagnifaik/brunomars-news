<?php
function dbconnect() {
	$user='root';
    $pass='root';
    $dsn='mysql:host=localhost;port=3306;dbname=brunomars';
    static $dbh = null;
    if ($dbh === null) {
        $dbh = new PDO($dsn, $user, $pass);
		$dbh->exec("set names utf8");
    }
    return $dbh;
}
// Users
function getUsers()
{
	$resultats=dbconnect()->query("SELECT * FROM Users");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function findUserById($id)
{
	$resultats=dbconnect()->query("SELECT * FROM Users WHERE id=".$id);
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
// NomSite
function getNomSite()
{
	$resultats=dbconnect()->query("SELECT * FROM NomSite");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
// Accueil
function getAccueil()
{
	$resultats=dbconnect()->query("SELECT * FROM Accueil");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function updateAccueil($image,$alt,$h1,$h2,$h22,$p,$url)
{
	dbconnect()->exec('UPDATE Accueil SET image="'.$image.'", alt="'.$alt.'", h1="'.$h1.'", h2="'.$h2.'", h22="'.$h22.'", p="'.$p.'", url="'.$url.'"');
}
// Biographie
function getBiographie()
{
	$resultats=dbconnect()->query("SELECT * FROM Biographie");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function updateBiographie($image,$alt,$h1,$h3,$h2,$h33,$h22,$h333,$url)
{
	dbconnect()->exec('UPDATE Biographie SET image="'.$image.'", alt="'.$alt.'", h1="'.$h1.'", h3="'.$h3.'", h2="'.$h2.'", h33="'.$h33.'", h22="'.$h22.'", h333="'.$h333.'", url="'.$url.'"');
}
// TourPage
function getTourPage()
{
	$resultats=dbconnect()->query("SELECT * FROM TourPage");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function updateTour($image,$alt,$h1,$h2,$h22,$image1,$alt1,$image2,$alt2,$url)
{
	dbconnect()->exec('UPDATE TourPage SET image="'.$image.'", alt="'.$alt.'", h1="'.$h1.'", h2="'.$h2.'", h22="'.$h22.'", image1="'.$image1.'", alt1="'.$alt1.'", image2="'.$image2.'", alt2="'.$alt2.'", url="'.$url.'"');
}
// Contact
function getContact()
{
	$resultats=dbconnect()->query("SELECT * FROM Contact");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function updateContact($image,$alt,$h1,$h2,$h3,$h33,$adresse,$phone,$email,$url)
{
	dbconnect()->exec('UPDATE Contact SET image="'.$image.'", alt="'.$alt.'", h1="'.$h1.'", h2="'.$h2.'", h3="'.$h3.'", h33="'.$h33.'", adresse="'.$adresse.'", phone="'.$phone.'", email="'.$email.'", url="'.$url.'"');
}
// Menu
function getMenu()
{
	$resultats=dbconnect()->query("SELECT * FROM Menu");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
//Album
function getAlbum()
{
	$resultats=dbconnect()->query("SELECT * FROM Album");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function getAlbumStatut($statut)
{
	$resultats=dbconnect()->query("SELECT * FROM Album WHERE statut=".$statut);
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function getAlbumById($id)
{
	$resultats=dbconnect()->query("SELECT * FROM Album WHERE id=".$id);
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function updateAlbumStatut($statut)
{
	dbconnect()->exec('UPDATE Album SET statut="'.$statut.'"');
}
function updateAlbumStatutById($id,$statut)
{
	dbconnect()->exec('UPDATE Album SET statut="'.$statut.'" WHERE id="'.$id.'"');
}
function insertAlbum($nom, $image, $daty, $statut)
{
	dbconnect()->exec('INSERT INTO Album (nom,image,daty,statut) VALUES ("'.$nom.'","'.$image.'","'.$daty.'","'.$statut.'")');
}

// Tour
function getTour()
{
	$resultats=dbconnect()->query("SELECT * FROM Tour");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function getTourById($id)
{
	$resultats=dbconnect()->query("SELECT * FROM Tour WHERE id=".$id);
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function updateTours($id,$daty,$pays,$lieu)
{
	dbconnect()->exec('UPDATE Tour SET daty="'.$daty.'", pays="'.$pays.'", lieu="'.$lieu.'" WHERE id='.$id);
}
function deleteTours($id)
{
	dbconnect()->exec("DELETE FROM Tour WHERE id='".$id."'");
}
// Chanson
function getChansons()
{
	$resultats=dbconnect()->query("SELECT * FROM Chanson ORDER BY id ASC");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function getChansonByIdAlbum($idAlbum)
{
	$resultats=dbconnect()->query("SELECT * FROM Chanson WHERE idAlbum=".$idAlbum);
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function getChansonById($id)
{
	$resultats=dbconnect()->query("SELECT * FROM Chanson WHERE id=".$id);
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function insertChanson($titre, $fichier, $album)
{
	dbconnect()->exec('INSERT INTO Chanson(titre,fichier,idAlbum) VALUES ("'.$titre.'","'.$fichier.'","'.$album.'")');
}
function updateMusique($id, $titre, $album)
{
	dbconnect()->exec('UPDATE Chanson SET titre="'.$titre.'", idAlbum="'.$album.'" WHERE id="'.$id.'"');
}
function deleteMusique($id)
{
	dbconnect()->exec("DELETE FROM Chanson WHERE id='".$id."'");
}
?>