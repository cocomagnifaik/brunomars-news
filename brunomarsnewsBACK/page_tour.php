<?php 
session_start();
include('fonctions.php'); 
$nomSites = getNomSite();
$tours = getTourPage();
$nomAlbums = NULL;
$names = NULL;
foreach($nomSites as $name){
	$names = $name['nom'];
}
    $users = findUserById($_SESSION["idUser"]);
	$pseudo = NULL;
	$image = NULL;
	foreach($users as $user){
		$pseudo = $user['pseudo'];
		$image = $user['image'];
	}
?>
<!DOCTYPE html>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BACKOFFICE</title>
          <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
          <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
          <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
          <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
          <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
 	<?php
		$albums = getAlbum();
		if(isset($_POST['titre']) && isset($_POST['categorie'])){
			insertChanson($_POST['titre'], $_POST['fich'], $_POST['categorie']);
	?>
			<script>alert('Insertion reussie')</script>
	<?php
		}
	?>
 <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include("header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
    <?php include("aside.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tours
        <small>Modification</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="page_musique.php"><i class="fa fa-dashboard"></i> Tours</a></li>
        <li class="active">Modifier</li>
      </ol>
    </section>
        <section class="col-md-12" style="margin-bottom: 50px">
			<div class="box-body col-md-12">
              <table class="table table-bordered">
                <tr>
                  <th width="14%">IMAGE</th>
                  <th width="14%">H1</th>
                  <th width="14%">H2</th>
                  <th width="14%">H2`</th>
                  <th width="14%">IMAGE1</th>
                  <th width="14%">IMAGE2</th>
                  <th width="14%">URL</th>
                </tr>
            <?php foreach($tours as $tour){ ?>
				<tr>
                  <td><img src="<?php echo $tour['image'];?>" width="100%"><br><?php echo $tour['alt'];?></td>
                  <td><?php echo $tour['h1'];?></td>
                  <td><?php echo $tour['h2'];?></td>
                  <td><?php echo $tour['h22'];?></td>
                  <td><img src="<?php echo $tour['image1'];?>" width="100%"><br><?php echo $tour['alt1'];?></td>
                  <td><img src="<?php echo $tour['image2'];?>" width="100%"><br><?php echo $tour['alt2'];?></td>
                  <td><?php echo $tour['url'];?></td>
				  <td><a href="page_tour_modifier.php?id=<?php echo $tour['id'];?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i>Modifier</a>
                </tr>
			<?php } ?>
              </table>
			</div>
        </section>			
	</div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Project by</b> Tsiory Fahendrena
    </div>
    <strong>Copyright &copy; 2018.</strong> All rights
    reserved.
  </footer>
</div>

<!-- jQuery 3 -->
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>