<?php 
session_start();
include('fonctions.php'); 
$nomSites = getNomSite();
$chansons = getChansons();
$nomAlbums = NULL;
$names = NULL;
foreach($nomSites as $name){
	$names = $name['nom'];
}
    $users = findUserById($_SESSION["idUser"]);
	$pseudo = NULL;
	$image = NULL;
	foreach($users as $user){
		$pseudo = $user['pseudo'];
		$image = $user['image'];
	}
?>
<!DOCTYPE html>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BACKOFFICE</title>
          <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
          <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
          <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
          <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
          <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
 	<?php
		$albums = getAlbum();
		if(isset($_POST['titre']) && isset($_POST['categorie'])){
			insertChanson($_POST['titre'], $_POST['fich'], $_POST['categorie']);
	?>
			<script>alert('Insertion reussie')</script>
	<?php
		}
	?>
 <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include("header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
    <?php include("aside.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Chanson
        <small>Insertion/Modification/Suppression</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="page_musique.php"><i class="fa fa-dashboard"></i> Accueil</a></li>
        <li class="active">Chanson</li>
      </ol>
    </section>
    <section class="content">
		<section class="col-md-12">
		  <div class="box box-info">
			<div class="small-box bg-red col-md-6">
				<h4><strong>Inserer d'abord votre fichier mp3 dans les champs suivants:</strong></h4><br>
			  <form name="validation">
				  <progress value="0" max="100" id="uploadProgress">0</progress> Veuillez attendre la fin du telechargement du <strong>fichier</strong> avant de "Valider votre fichier"!
				<input type="file" class="form-control" name="fichier" id="fichier" placeholder="Fichier" required>
				<br>
				<button type="button" id="valid" onclick="go()" style="background: #005983" data-widget="remove">Valider votre chanson</button>
			  </form><br>
			</div>
		  </div>
		</section>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyDZWbV5WwKOIEwRZ0Nq9POO2YKxrLxaw3M",
    authDomain: "brunomarsnews-512fe.firebaseapp.com",
    databaseURL: "https://brunomarsnews-512fe.firebaseio.com",
    projectId: "brunomarsnews-512fe",
    storageBucket: "brunomarsnews-512fe.appspot.com",
    messagingSenderId: "1002209203404"
  };
  firebase.initializeApp(config);
  var storage = firebase.storage();  
  var progress = document.getElementById('uploadProgress') 
  var button = document.getElementById('fichier')
  var valid = document.getElementById('valid')
    button.addEventListener('change',function(e){
	var file = e.target.files[0];
	var storageRef = storage.ref(file.name)
	var uploadTask = storageRef.put(file)	
	uploadTask.on('state_changed',loadUpload,errUpload,completeUpload)	
	function loadUpload(data){
		var percent = (data.bytesTransferred/data.totalBytes) * 100
		progress.value = percent
	}	
	function errUpload(err){
		console.log('error')
		console.log(err)
	}
	function completeUpload(data){
		alert('Succes')
		console.log('success')
		console.log(data)
	}
  })
  var refDown = null
    function go(){
        var ato = document.getElementById('fichier').value
        var tableau = ato.split("\\")
        refDown = storage.ref(tableau[2])
        alert('Fichier mp3 valide!')
    }
        valid.addEventListener('click',function(){
            refDown.getDownloadURL().then(function(url){
                console.log("MP3 download")
                console.log(url)
                document.getElementById("fich").value=url
            })
        })
</script>
        <section class="col-md-12" style="margin-bottom: 50px">
            <div class="box-body col-md-6" style="background: #003147; color: #FFF">
                <h4><strong>Inserer ensuite les details de la chanson:</strong></h4>
              <form action="page_musique.php" method="post">
                <div class="input-group form-group">
                   <span class="input-group-addon" id="sizing-addon-2">Album</span>
                    <select class="form-control" name="categorie" id="categorie" aria-describedby="sizing-addon-2" required>
					<?php foreach($albums as $album){ ?>
                        <option value="<?php echo $album['id']; ?>"><?php echo $album['nom']; ?></option>
					<?php } ?>
                    </select>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="titre" placeholder="Titre" required>
                </div>
                <div class="form-group">
                  <input type="hidden" class="form-control" name="fich" id="fich">
                </div>
                <button type="submit" class="pull-right btn btn-success" id="insert" name="insert">Inserer<i class="fa fa-arrow-circle-right"></i></button>
              </form>
            </div>
			<div class="box-body col-md-12">
              <table class="table table-bordered">
                <tr>
                  <th>Titre</th>
                  <th>Fichier</th>
                  <th>Album</th>
                </tr>
            <?php foreach($chansons as $chanson){ 
				$nomAlbums = getAlbumById($chanson['idAlbum']);
				foreach($nomAlbums as $nomAlbum){
			?>
				<tr>
                  <td><?php echo $chanson['titre'];?></td>
                  <td><?php echo $chanson['fichier'];?></td>
                  <td><?php echo $nomAlbum['nom'];?></td>
				  <td><a href="page_musique_modifier.php?id=<?php echo $chanson['id'];?>" class="btn btn-info btn-xs" width="400px"><i class="fa fa-pencil"></i>Modifier</a>
				  <br><a href="page_musique_delete.php?id=<?php echo $chanson['id'];?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i>Supprimer</a></td>
                </tr>
			<?php }
			}?>
              </table>
			</div>
        </section>
    </section>
		</div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Project by</b> Tsiory Fahendrena
    </div>
    <strong>Copyright &copy; 2018.</strong> All rights
    reserved.
  </footer>
</div>

<!-- jQuery 3 -->
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>