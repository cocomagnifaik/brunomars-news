<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image;?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $pseudo;?></p>
          <i class="fa fa-circle text-success"></i> Online
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Utils</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="page_musique.php"><i class="fa fa-circle-o"></i> Chanson</a></li>
            <li><a href="page_album.php"><i class="fa fa-circle-o"></i> Album</a></li>
            <li><a href="page_concert.php"><i class="fa fa-circle-o"></i> Concerts</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Modifier Pages</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="page_accueil.php"><i class="fa fa-circle-o"></i>Accueil</a></li>
            <li><a href="page_biographie.php"><i class="fa fa-circle-o"></i>Biographie</a></li>
            <li><a href="page_tour.php"><i class="fa fa-circle-o"></i>Tour</a></li>
            <li><a href="page_contact.php"><i class="fa fa-circle-o"></i>Contact</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>