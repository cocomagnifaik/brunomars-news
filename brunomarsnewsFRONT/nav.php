	<?php
		$menus = getMenu();
		$nomSites = getNomSite();
		$names = NULL;
		foreach($nomSites as $name){
			$names = $name['nom'];
		}
	?>
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="index.php"><?php echo $names; ?></a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>
					<?php foreach($menus as $menu){ ?>
						<li><a href="<?php echo $menu['page'];?>.jsp"><?php echo $menu['nom'];?></a></li>
					<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</nav>