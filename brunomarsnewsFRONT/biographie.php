<?php 
	include('fonctions.php');
	$biographies = getBiographie();
	$h1 = NULL;
	$h3 = NULL;
	$h2 = NULL;
	$h33 = NULL;
	$h22 = NULL;
	$h333 = NULL;
	$url = NULL;
	foreach($biographies as $biographie){
		$h1 = $biographie['h1'];
		$h3 = $biographie['h3'];
		$h2 = $biographie['h2'];
		$h33 = $biographie['h33'];
		$h22 = $biographie['h22'];
		$h333 = $biographie['h333'];
		$url = $biographie['url'];
	}
	if(strtolower($_GET['url']) != $url){
		header('location:'.$url.'.jsp');
	}
?>
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $h1.', '.$h2; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Bruno Mars est un artiste Americain ayant sorti deja 3 albums. 24K Magic en est son troisième sorti en 2016. Le premier single extrait de cet album est 24K Magic sorti le 7 octobre 2016." />
	<meta name="keywords" content="bruno mars album, bruno news, 24k magic, bruno mars, free website template, album, 2018, album 2016, music" />
	<meta name="author" content="Coco Magnifaik" />
	 <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	
	<div class="gtco-loader"></div>
	
	<div id="page">

	<div class="page-inner">
	<?php include('nav.php');?>
	
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/Contact.jpg); height:50%">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<h1><?php echo $h1;?></h1><br>
							<h3><?php echo $h3;?></h3>
							<h2 style="font-size:50px;"><?php echo $h2;?></h2>
							<h3><?php echo $h33;?></h3>
							<h2 style="font-size:50px;"><?php echo $h22;?></h2>
							<h3><?php echo $h333;?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
		<?php include('footer.php'); ?>
	</div>

	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>
