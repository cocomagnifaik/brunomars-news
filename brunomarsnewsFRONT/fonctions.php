<?php
function dbconnect() {
	$user='root';
    $pass='root';
    $dsn='mysql:host=localhost;port=3306;dbname=brunomars';
    static $dbh = null;
    if ($dbh === null) {
        $dbh = new PDO($dsn, $user, $pass);
		$dbh->exec("set names utf8");
    }
    return $dbh;
}
// NomSite
function getNomSite()
{
	$resultats=dbconnect()->query("SELECT * FROM NomSite");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
// Accueil
function getAccueil()
{
	$resultats=dbconnect()->query("SELECT * FROM Accueil");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
// Biographie
function getBiographie()
{
	$resultats=dbconnect()->query("SELECT * FROM Biographie");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
// TourPage
function getTourPage()
{
	$resultats=dbconnect()->query("SELECT * FROM TourPage");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
// Contact
function getContact()
{
	$resultats=dbconnect()->query("SELECT * FROM Contact");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
// Menu
function getMenu()
{
	$resultats=dbconnect()->query("SELECT * FROM Menu");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
//Album
function getAlbum()
{
	$resultats=dbconnect()->query("SELECT * FROM Album");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function getAlbumStatut($statut)
{
	$resultats=dbconnect()->query("SELECT * FROM Album WHERE statut=".$statut);
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
// Chanson
function getChansons()
{
	$resultats=dbconnect()->query("SELECT * FROM Chanson ORDER BY id ASC");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
function getChansonByIdAlbum($idAlbum)
{
	$resultats=dbconnect()->query("SELECT * FROM Chanson WHERE idAlbum=".$idAlbum);
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
// Tour
function getTour()
{
	$resultats=dbconnect()->query("SELECT * FROM Tour");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}
?>