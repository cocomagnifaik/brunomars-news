<?php 
	include('fonctions.php');
	$albums = getAlbum();
	$albumThemes = getAlbumStatut('1');
	$theme = NULL;
	$idAlbum = NULL;
	foreach($albumThemes as $albumTheme){
		$theme = $albumTheme['image'];
		$idAlbum = $albumTheme['id'];
	}
	$chansons = getChansonByIdAlbum($idAlbum);
	$h1 = NULL;
	$h2 = NULL;
	$h22 = NULL;
	$p = NULL;
	$url = NULL;
	$accueils = getAccueil();
	foreach($accueils as $accueil){
		$h1 = $accueil['h1'];
		$h2 = $accueil['h2'];
		$h22 = $accueil['h22'];
		$p = $accueil['p'];
		$url = $accueil['url'];
	}
	if(strtolower($_GET['url']) != $url){
		header('location:'.$url.'.jsp');
	}
?>
<!DOCTYPE html>
<html>
		<style>
			#playlist{
				list-style: none;
			}
			#playlist li a{
				color:black;
				text-decoration: none;
			}
			.scroll {
				overflow-y: scroll;
				height: 100px;
			}
		</style>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $h1.', '.$h2; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Bruno Mars est un artiste Americain ayant sorti deja 3 albums. 24K Magic en est son troisième sorti en 2016. Le premier single extrait de cet album est 24K Magic sorti le 7 octobre 2016." />
	<meta name="keywords" content="bruno mars album, bruno news, 24k magic, bruno mars, free website template, album, 2018, album 2016, music" />
	<meta name="author" content="Coco Magnifaik" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

	<div class="page-inner">

	<?php include('nav.php');?>
	<header id="gtco-header" class="gtco-cover" role="banner" style="background-image: url(images/walp1.jpg); height:50%">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row" style="margin-top:15%;">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">SORTIE OFFICIELLE</span>
							<h1><?php echo $h1;?></h1>
							<h2><?php echo $h2;?></h2>
						</div>
						<div class="col-md-4 col-md-push-1 animate-box" data-animate-effect="fadeInRight">
							<div class="form-wrap" style="margin-bottom:10%;">
								<div class="tab">
									<div class="tab-content">
										<div class="tab-content-inner active" data-content="albums">
										<p align="center">ALBUM EN INTEGRALITE</p>
											<div class="row form-group">
												<div class="col-md-12">
													<img src="<?php echo $theme;?>" width="100%">
												</div>
											</div>
											<div class="row form-group">
												<div class="col-md-12" id="audio_player_box">
													<div id="audio_controls_bar">
													<audio src="" controls id="audioPlayer" style="width:100%">
													</audio>
												<div class="scroll">
											<?php foreach($chansons as $chanson){ ?>
													<ul id="playlist">
														<li class="current-song"><a href="<?php echo $chanson['fichier'];?>"><p style="color:#666767"><img src="images/play.png" width="10%"><?php echo $chanson['titre'];?></p></a></li>
													</ul>
											<?php } ?>
												</div>
													<script src="jqueryGG.js">	
													</script>
													<script>
														audioPlayer();
														function audioPlayer(){
															var currentSong = 0;
															$("#audioPlayer")[0].src = $("playlist li a")[0];
															$("#audioPlayer")[0].play();
															$("#playlist li a").click(function(e){
																e.preventDefault();
																$("#audioPlayer")[0].src = this;
																$("#audioPlayer")[0].play();
																$("#playlist li").removeClass("current-song");
																currentSong = $(this).parent().index();
																$(this).parent.addClass("current-song");
															});
															$("#audioPlayer")[0].addEventListener("ended",function(){
																currentSong++;
																if(currentSong == $("#playlist li a").length)
																	currentSong = 0
																$("#playlist li").removeClass("current-song");
																$("#playlist li:eq("+currentSong+")").addClass("current-song");
																$("#audioPlayer")[0].src = $("#playlist li a")[currentSong].href;
																$("#audioPlayer")[0].play();
															});
														}
													</script>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<div class="gtco-section border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2><?php echo $h22; ?></h2>
					<p><?php echo $p; ?></p>
				</div>
			</div>
			<div class="row">
			<?php foreach($albums as $album){ ?>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="<?php echo $album['image']?>" class="fh5co-project-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="<?php echo $album['image']?>" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2><?php echo $album['nom']?></h2>
							<p>Ann&eacute;e de sortie: <?php echo $album['daty']?></p>
						</div>
					</a>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
	<?php include('footer.php'); ?>
	</div>

	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>

