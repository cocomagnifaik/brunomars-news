CREATE TABLE NomSite(
	id INT,
	nom VARCHAR(100)
);
INSERT INTO NomSite VALUES(1, 'B<em>.</em>M News');
CREATE TABLE Menu(
	id INT,
	nom VARCHAR(10),
	page VARCHAR(20)
);
INSERT INTO Menu VALUES(1,'Biographie','biographie-bruno-mars-un-phenomene-international');
INSERT INTO Menu VALUES(2,'Tour','tour-page-bruno-mars');
INSERT INTO Menu VALUES(3,'Contact','contact-contacter-nous');

CREATE TABLE Album(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50),
	image VARCHAR(500),
	daty DATE,
	statut VARCHAR(10)
);
INSERT INTO Album VALUES('1','Doo-Wops & Hooligans','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/DHl.jpg?alt=media&token=aa111d0e-33a0-4342-b633-e051eb10af25','2010-08-04','0');
INSERT INTO Album VALUES('2','Unorthodox Jukebox','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/BrunoMarsUJ.png?alt=media&token=78156dc1-2780-4c5c-b8aa-2453f33b1e64','2012-12-07','0');
INSERT INTO Album VALUES('3','24K MAGIC','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/24k.jpeg?alt=media&token=29afafb3-a79f-4350-9c96-665e412cf871','2016-08-07
','1');

CREATE TABLE Chanson(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	titre VARCHAR(50),
	fichier VARCHAR(500),
	idAlbum INT,
	FOREIGN KEY (idAlbum) REFERENCES Album(id)
);
INSERT INTO Chanson VALUES('1','24K Magic','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/Bruno%20Mars%20-%2024K%20Magic.mp3?alt=media&token=06dad612-9bde-4904-af71-de34e42dfb96','3');
INSERT INTO Chanson VALUES('2','Chunky','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/(1)%20Bruno%20Mars%20-%20Chunky%20%5BOfficial%20Audio%5D%20-%20YouTube.mp3?alt=media&token=dca04826-36f0-4914-aad0-2f7deb1a0415','3');
INSERT INTO Chanson VALUES('3','Perm','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/Bruno%20Mars%20-%20Perm%20%5BOfficial%20Audio%5D%20-%20YouTube.mp3?alt=media&token=c6bfdf44-0c8f-4d8e-991e-437ea7e02ff2','3');
INSERT INTO Chanson VALUES('4','That\'s What I Like','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/(1)%20Bruno%20Mars%20-%20That%E2%80%99s%20What%20I%20Like%20%5BOfficial%20Video%5D%20-%20YouTube.mp3?alt=media&token=46e05f76-2254-48b8-941c-d77654065993','3');
INSERT INTO Chanson VALUES('5','Versace on the Floor','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/(1)%20Bruno%20Mars%20-%20Versace%20On%20The%20Floor%20%5BOfficial%20Video%5D%20-%20YouTube.mp3?alt=media&token=1d395212-b488-43ad-b07d-f676db6014a5','3');
INSERT INTO Chanson VALUES('6','Straight Up & Down','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/(1)%20Bruno%20Mars%20-%20Versace%20On%20The%20Floor%20%5BOfficial%20Video%5D%20-%20YouTube.mp3?alt=media&token=1d395212-b488-43ad-b07d-f676db6014a5','3');
INSERT INTO Chanson VALUES('7','Calling All My Lovelies','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/(1)%20Bruno%20Mars%20-%20Calling%20All%20My%20Lovelies%20(Audio)%20-%20YouTube.mp3?alt=media&token=08862dbc-1c10-4d5d-b6a7-94f5933c54fb','3');
INSERT INTO Chanson VALUES('8','Finesse','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/(1)%20Bruno%20Mars%20-%20Finesse%20%5BOfficial%20Audio%5D%20-%20YouTube.mp3?alt=media&token=abc57a0a-6d51-4454-b45f-d90c40b7786e','3');
INSERT INTO Chanson VALUES('9','Too Good to Sy Goodbye','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/(1)%20Bruno%20Mars%20-%20Too%20Good%20To%20Say%20Goodbye!%20(AUDIO)%20-%20YouTube.mp3?alt=media&token=f51e8246-984e-4759-af12-aaaf4ace6b31','3');

CREATE TABLE Tour(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	daty DATE,
	pays VARCHAR(50),
	lieu VARCHAR(50)
);
INSERT INTO Tour VALUES('1','2018/09/07','DENVER, CO','PEPSI CENTER');
INSERT INTO Tour VALUES('2','2018/09/08','DENVER, CO','PEPSI CENTER');
INSERT INTO Tour VALUES('3','2018/09/11','ST PAUL, MN','XCEL ENERGY CENTER');
INSERT INTO Tour VALUES('4','2018/09/12','ST PAUL, MN','XCEL ENERGY CENTER');
INSERT INTO Tour VALUES('5','2018/09/15','DETROIT, MI','LITTLE CAESATS ARENA');
INSERT INTO Tour VALUES('6','2018/09/16','DETROIT, MI','LITTLE CAESATS ARENA');
INSERT INTO Tour VALUES('7','2018/09/19','PHILADELPHIA, PA','WELLS FARGO CENTER');
INSERT INTO Tour VALUES('8','2018/09/20','PHILADELPHIA, PA','WELLS FARGO CENTER');
INSERT INTO Tour VALUES('9','2018/09/22','TORONTO, ON','AIR CANADA CENTRE');
INSERT INTO Tour VALUES('10','2018/09/23','TORONTO, ON','AIR CANADA CENTRE');
INSERT INTO Tour VALUES('11','2018/09/27','BOSTON, MA','TO GARDEN');
INSERT INTO Tour VALUES('12','2018/09/28','BOSTON, MA','TO GARDEN');
INSERT INTO Tour VALUES('13','2018/08/01','NEWARK, NJ','PRUDENTIAL CENTER');
INSERT INTO Tour VALUES('14','2018/08/02','NEWARK, NJ','PRUDENTIAL CENTER');
INSERT INTO Tour VALUES('15','2018/08/04','BROOKLYN, NY','BARCLAYS CENTER');
INSERT INTO Tour VALUES('16','2018/08/05','BROOKLYN, NY','BARCLAYS CENTER');
INSERT INTO Tour VALUES('17','2018/08/07','NASHVILLE, TN,','BRIDGESTONE ARENA');
INSERT INTO Tour VALUES('18','2018/08/11','TULSA OK','BOK CENTER');
INSERT INTO Tour VALUES('19','2018/08/12','TULSA OK','BOK CENTER');
INSERT INTO Tour VALUES('20','2018/08/14','DALLAS, TX','AMERICAN AIRLINES CENTER');
INSERT INTO Tour VALUES('21','2018/08/15','DALLAS, TX','AMERICAN AIRLINES CENTER');
INSERT INTO Tour VALUES('22','2018/08/23','LOS ANGELES, CA','STAPLES CENTER');
INSERT INTO Tour VALUES('23','2018/08/24','LOS ANGELES, CA','STAPLES CENTER');
INSERT INTO Tour VALUES('24','2018/08/26','LOS ANGELES, CA','STAPLES CENTER');
INSERT INTO Tour VALUES('25','2018/08/27','LOS ANGELES, CA','STAPLES CENTER');

CREATE TABLE Accueil(
	id INT,
	image VARCHAR(500),
	alt VARCHAR(200),
	h1 VARCHAR(100),
	h2 VARCHAR(500),
	h22 VARCHAR(100),
	p VARCHAR(100),
	url VARCHAR(200)
);
INSERT INTO Accueil VALUES(1,'https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/walp1.jpg?alt=media&token=1e42db25-97de-4bb8-9fb2-f9bb13deb1f8','bruno mars 24k magic','Nouvel album de Bruno Mars<br> 24K Magic','24K Magic est le troisième album studio du chanteur américain Bruno Mars sorti en 2016, publié chez Atlantic Records. Le premier single extrait de cet album est 24K Magic sorti le 7 octobre 2016.','SES ALBUMS <br> avant 24K Magic','De 2009 à 2018','accueil-nouvel-album-de-bruno-mars-24k-magic');
CREATE TABLE Biographie(
	id INT,
	image VARCHAR(500),
	alt VARCHAR(200),
	h1 VARCHAR(100),
	h3 VARCHAR(1000),
	h2 VARCHAR(500),
	h33 VARCHAR(800),
	h22 VARCHAR(500),
	h333 VARCHAR(500),
	url VARCHAR(200)
);
INSERT INTO Biographie VALUES(1,'https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/Contact.jpg?alt=media&token=767f48fb-ce5a-4dad-b33f-15001d470e0d','contact bruno mars news','Bruno Mars, un phenomene international','Peter Gene Hernandez(dit Bruno Mars), né le 8 octobre 1985 à Honolulu (Hawaï), est un auteur-compositeur-interprète et producteur américain. Il grandit dans une famille de musiciens et fait ses débuts dans la musique en produisant d\'autres artistes, au travers de l\'équipe de production The Smeezingtons avec Philip Lawrence et Ari Levine.','Sa carrière d\'artiste...','Il se fait connaître avec le titre Nothin\' on You de B.o.B, puis Billionaire de Travie McCoy, avant la sortie mondiale de son premier album Doo-Wops & Hooligans en 2010. Il coécrit également de nombreux titres dont Right Round de Flo Rida, Wavin\' Flag de K\'naan ou Fuck You! de Cee Lo Green. En 2012, son deuxième album Unorthodox Jukebox est numéro un aux États-Unis. Il contient les chansons Locked Out of Heaven, When I Was Your Man et Treasure, qui sont des succès internationaux. En 2014, il intervient sur le morceau Uptown Funk de Mark Ronson. Deux ans plus tard, il sort son troisième album intitulé 24K Magic.','De quel genre de musique s\'inspire-t-il?','Enfant, il est très influencé par des artistes comme Elvis Presley, James Brown et Michael Jackson. Bruno Mars s\'inspire également du reggae et des sons de la Motown. C\'est d\'ailleurs gr&acirc;ce à cela que son style de musique est un mélange de différents styles musicaux.','biographie-bruno-mars-un-phenomene-international');
CREATE TABLE TourPage(
	id INT,
	image VARCHAR(500),
	alt VARCHAR(200),
	h1 VARCHAR(100),
	h2 VARCHAR(100),
	h22 VARCHAR(100),
	image1 VARCHAR(500),
	alt1 VARCHAR(200),
	image2 VARCHAR(500),
	alt2 VARCHAR(200),
	url VARCHAR(200)
);
INSERT INTO TourPage VALUES(1,'https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/img_44.jpg?alt=media&token=697a52ce-d210-4d17-8223-485fbc6d3a31','bruno mars jpg','BRUNO MARS','XXIVK MAGIC','TOURNEE MONDIALE','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/bruno.png?alt=media&token=5aba0890-be99-410b-a531-c160dcf0eb79','bruno mars png','https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/cardi.png?alt=media&token=a8dfae4f-7c29-4a80-8b95-36521a04edc0','cardi b.png','tour-page-bruno-mars');
CREATE TABLE Contact(
	id INT,
	image VARCHAR(500),
	alt VARCHAR(200),
	h1 VARCHAR(100),
	h2 VARCHAR(500),
	h3 VARCHAR(100),
	h33 VARCHAR(100),
	adresse VARCHAR(100),
	phone VARCHAR(50),
	email VARCHAR(50),
	url VARCHAR(200)
);
INSERT INTO Contact VALUES(1,'https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/Contact.jpg?alt=media&token=7ffa8e66-30e4-4b5d-8065-036c114369c0','contact bruno mars','contacter','N\'hésitez pas à nous contacter pour plus d\'information','Champs à compléter','Contact Information','198 West 21th Street, <br> Suite 721 New York NY 10016','+261 34 91 251 70','bentsiory@gmail.com','contact-contacter-nous');

CREATE TABLE Users(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(100),
	pseudo VARCHAR(50),
	password VARCHAR(100),
	image VARCHAR(500)
);
INSERT INTO Users VALUES(1,'bentsiory@gmail.com','Coco',sha1('coco'),'https://firebasestorage.googleapis.com/v0/b/brunomarsnews-512fe.appspot.com/o/Coco.jpg?alt=media&token=6cc4909b-1e86-4758-b175-7c2a43c6e0e4');