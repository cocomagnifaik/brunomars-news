	<footer>
		<div class="row copyright">
			<div class="col-md-12">
				<p class="pull-left">
					<small class="block">&copy; 2018. All Rights Reserved.</small> 
					<small class="block">Designed by <a href="https://www.facebook.com/tsiur.rakotozandry" target="_blank">Tsiory Fahendrena Rakotozandry</a></small>
				</p>
				<p class="pull-right" align="center">
					<ul id="navlist">
					  <li id="facebook"><a href="https://www.facebook.com/brunomars/"></a></li>
					  <li id="instagram"><a href="https://www.instagram.com/brunomars/?hl=fr"></a></li>
					</ul>
				</p>
			</div>
		</div>
	</footer>
