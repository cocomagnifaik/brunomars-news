<?php 
	include('fonctions.php');
	$tourPages = getTourPage();
	$image = NULL;
	$h1 = NULL;
	$h2 = NULL;
	$h22 = NULL;
	$image1 = NULL;
	$alt1 = NULL;
	$image2 = NULL;
	$alt2 = NULL;
	$url = NULL;
	foreach($tourPages as $tourPage){
		$image = $tourPage['image'];
		$h1 = $tourPage['h1'];
		$h2 = $tourPage['h2'];
		$h22 = $tourPage['h22'];
		$image1 = $tourPage['image1'];
		$alt1 = $tourPage['alt1'];
		$image2 = $tourPage['image2'];
		$alt2 = $tourPage['alt2'];
		$url = $tourPage['url'];
	}
	$tours = getTour();
	if(strtolower($_GET['url']) != $url){
		header('location:'.$url.'.jsp');
	}
?>
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $h1.' '.$h2.' '.$h22; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Bruno Mars est un artiste Americain ayant sorti deja 3 albums. 24K Magic en est son troisième sorti en 2016. Le premier single extrait de cet album est 24K Magic sorti le 7 octobre 2016." />
	<meta name="keywords" content="bruno mars album, bruno news, 24k magic, bruno mars, free website template, album, 2018, album 2016, music" />
	<meta name="author" content="Coco Magnifaik" />
	<!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->
  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

	<div class="page-inner">
	<?php include('nav.php');?>
	
	<header id="gtco-header" class="" role="banner" style="background: #001">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row" style="background-image:url(<?php echo $image;?>);background-repeat: no-repeat;width:110%; margin-top:15%;">
						<div class="col-md-12 mt-text animate-box" align="center" data-animate-effect="fadeInUp">
							<span class="intro-text-small"></span>
							<h1 style="margin-top:-110px; margin-left:-60px; font-size:160px; text-shadow: -5px 0 white,0 5px white, 5px 0 white, 0 -5px white; font-family:Courier;color:black;"><strong><?php echo $h1; ?></strong></h1>	
						</div>						
					</div>
				</div>
			</div>
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row">
						<div class="col-md-12" align="center">
							<h2 style="margin-top:-70px;font-size:70px;font-family:Courier;"><strong><?php echo $h2; ?></strong></h2>	
						</div>						
					</div>
				</div>
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row" style="background:#e52d27;margin-top:-50px; height:60px;">
						<div class="col-md-12" align="center">
							<h2 style="margin-top:-20px;font-size:70px;text-shadow: -5px 0 #f4c746,0 5px #f4c746, 5px 0 #f4c746, 0 -5px #f4c746;font-family:Courier; color:black"><strong><?php echo $h22; ?></strong></h2>	
						</div>						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="row">
						<img src="<?php echo $image1; ?>" width="90%" alt="<?php echo $alt1;?>" title="<?php echo $alt1;?>">
					</div>
				</div>
				<div class="col-md-6">
				<?php foreach($tours as $tour){ ?>
					<div class="row">
						<?php echo '<strong style="color:#e52d27">'.strftime('%A %m %B', strtotime($tour['daty'])).'</strong>';?>
						<?php echo '<strong style="color:#FFF">'.$tour['pays'].'</strong>';?>
						<?php echo '<strong style="color:#f4c746">'.$tour['lieu'].'</strong>'; ?>
					</div>
				<?php } ?>
				</div>
				<div class="col-md-3">
					<div class="row">
						<img src="<?php echo $image2; ?>" width="90%" alt="<?php echo $alt2;?>" title="<?php echo $alt2;?>">
					</div>
				</div>
			</div>
		</div>
	</header>

	<?php include('footer.php'); ?>
	</div>

	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>

